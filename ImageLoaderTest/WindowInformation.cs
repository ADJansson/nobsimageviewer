﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ImageLoaderTest
{
    public class WindowInformation
    {

        #region Constructor

        /// <summary>
        /// Initialize the class.
        /// </summary>
        public WindowInformation() { }

        #endregion

        #region Properties

        /// <summary>
        /// The window caption.
        /// </summary>
        public string Caption = string.Empty;

        /// <summary>
        /// The window class.
        /// </summary>
        public string Class = string.Empty;

        /// <summary>
        /// Children of the window.
        /// </summary>
        public List<WindowInformation> ChildWindows = new List<WindowInformation>();

        /// <summary>
        /// Unmanaged code to get the process and thres IDs of the window.
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="processId"></param>
        /// <returns></returns>
        [DllImport("user32")]
        private static extern int GetWindowThreadProcessId(IntPtr hWnd, out int processId);

        /// <summary>
        /// The string representation of the window.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "Window " + this.Handle.ToString() + " \"" + this.Caption + "\" " + this.Class;
        }

        /// <summary>
        /// The handles of the child windows.
        /// </summary>
        public List<IntPtr> ChildWindowHandles
        {
            get
            {
                try
                {
                    var handles = from c in this.ChildWindows.AsEnumerable()
                                  select c.Handle;
                    return handles.ToList<IntPtr>();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// The window handle.
        /// </summary>
        public IntPtr Handle;

        /// <summary>
        /// The parent window.
        /// </summary>
        public WindowInformation Parent { get; set; }

        /// <summary>
        /// The handle of the parent of the window.
        /// </summary>
        public IntPtr ParentHandle
        {
            get
            {
                if (this.Parent != null) return this.Parent.Handle;
                else return IntPtr.Zero;
            }
        }

        /// <summary>
        /// The corresponding process.
        /// </summary>
        public Process Process
        {
            get
            {
                try
                {
                    int processID = 0;
                    GetWindowThreadProcessId(this.Handle, out processID);
                    return Process.GetProcessById(processID);
                }
                catch (Exception ex) { return null; }
            }
        }

        /// <summary>
        /// Sibling window information.
        /// </summary>
        public List<WindowInformation> SiblingWindows = new List<WindowInformation>();

        /// <summary>
        /// The handles of the sibling windows.
        /// </summary>
        public List<IntPtr> SiblingWindowHandles
        {
            get
            {
                try
                {
                    var handles = from s in this.SiblingWindows.AsEnumerable()
                                  select s.Handle;
                    return handles.ToList<IntPtr>();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// The thread ID of the window. Returns -1 on exception.
        /// </summary>
        public int ThreadID
        {
            get
            {
                try
                {
                    int dummy = 0;
                    return GetWindowThreadProcessId(this.Handle, out dummy);
                }
                catch (Exception ex) { return -1; }
            }
        }

        #endregion
    }
}
