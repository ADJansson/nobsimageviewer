﻿using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using Microsoft.Web.WebView2.Core;

namespace ImageLoaderTest
{
    /// <summary>
    /// TODO: yes.
    /// </summary>
    public partial class ImageViewerForm : Form
    {
        private int _currentIndex = 0; // Keeps track of current image in directory
        private Image _currentImage;
        private FileInfo[] _files;
        private string _startFile;
        // private double _scaleFactor = 1; // Keep track of current zoom factor
        // private double _origScaleFactor = 1;

        private Point _mouseStart;
        private bool _mouseHold = false;
        private bool _ready = false;
        private WindowInfo _wInfo;

        private const string _settingsFile = "\\settings.json";
        private const string _rotationDataFile = "\\rotations.bin";

        public delegate void FormTextUpdateDelegate(string formText); // Delegates for cross-thread operations
        public delegate void CursorUpdateDelegate(Cursor cursor);

        // image rotation data
        private Dictionary<string, RotateFlipType> _rotationData = new Dictionary<string, RotateFlipType>();

        private Thread _mouseThread;
        private int _mouseTimer = 0;
        private Point _mouseStartHide;
        private bool _modifierKeyPressed = false;

        private Thread _slideThread;
        private int _slideDirection = 0; // No slide-show
        private int _slideTimer = 1000;

        public ImageViewerForm(string startFile)
        {
            InitializeComponent();
            Init(startFile, false);
        }

        public ImageViewerForm(string startFile, bool fromSearch, string searchName)
        {
            InitializeComponent();
            Init(startFile, fromSearch, searchName);
        }

        private async void Init(string startFile, bool fromSearch, string searchName = "")
        {
            HideErrorLabel();

            _startFile = startFile;

            MouseWheel += Form1_MouseWheel;

            webView21.Visible = false;
            imageBox1.SizeMode = PictureBoxSizeMode.CenterImage;

            var settingsFile = AppDomain.CurrentDomain.BaseDirectory + _settingsFile;
            if (File.Exists(settingsFile))
            {
                var js = new JavaScriptSerializer();
                var was = File.ReadAllText(settingsFile);
                _wInfo = js.Deserialize<WindowInfo>(was);

                var wazsd = new Point(_wInfo.location.X, _wInfo.location.Y); // dafuk

                WindowState = _wInfo.state;
                Size = _wInfo.size;

                StartPosition = FormStartPosition.Manual;
                Location = wazsd;
            }

            await webView21.EnsureCoreWebView2Async();
            LoadImage(_startFile);

            webView21.CoreWebView2.Settings.AreDevToolsEnabled = false;
            webView21.CoreWebView2.Settings.AreDefaultContextMenusEnabled = false;

            var rotationDataFile = AppDomain.CurrentDomain.BaseDirectory + _rotationDataFile;
            if (File.Exists(rotationDataFile))
            {
                try
                {
                    IFormatter formatter = new BinaryFormatter();
                    using (Stream stream = new FileStream(rotationDataFile, FileMode.Open, FileAccess.Read))
                    {
                        _rotationData = (Dictionary<string, RotateFlipType>)formatter.Deserialize(stream);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Could not load rotation data: " + e.Message);
                }
            }
            if (_rotationData.ContainsKey(_startFile))
            {
                imageBox1.ApplyRotation(_rotationData[_startFile]);
            }

            Thread load = new Thread(() =>
            {
                //Stopwatch st = new Stopwatch();
                //st.Start();

                // File opened from search
                if (fromSearch && Regex.IsMatch(_startFile, searchName, RegexOptions.IgnoreCase))
                {
                    _files = new DirectoryInfo(GetDirectory(startFile)).GetFiles(searchName).OrderBy(f => f.LastWriteTime).ToArray();
                }
                else
                {
                    _files = new DirectoryInfo(GetDirectory(startFile)).GetFiles("*").OrderBy(f => f.LastWriteTime).ToArray();
                }
                
                //st.Stop();
                //Debug.WriteLine("It took " + st.Elapsed + "ms");

                _currentIndex = Array.IndexOf(_files, _files.Where(f => f.FullName.Equals(startFile)).FirstOrDefault());

                if (_currentIndex == -1)
                {
                    if (_files.Length > 0)
                    {
                        while (!CompExt(_files[++_currentIndex].FullName, startFile))
                        {

                        }

                        //DisplayNext();
                    }
                }

                _ready = true;
            });

            load.IsBackground = true;
            load.Start();

            ThreadStart tsm = new ThreadStart(MouseLoop);
            _mouseThread = new Thread(tsm);
            _mouseThread.IsBackground = true;
            _mouseThread.Start();

            ThreadStart tss = new ThreadStart(SlideLoop);
            _slideThread = new Thread(tss);
            _slideThread.IsBackground = true;
            _slideThread.Start();
        }

        private void MouseLoop()
        {

            while (true)
            {
                _mouseTimer++;
                _mouseStartHide = Cursor.Position;

                // Hide cursor after 3 seconds
                if (_mouseTimer > 3)
                {
                    try
                    {
                        Cursor blank = new Cursor(Properties.Resources.blank.Handle);
                        Invoke(new CursorUpdateDelegate(SetCursor), new object[] { blank });
                    }
                    catch(Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                }

                Thread.Sleep(1000);
            }
        }

        private void SlideLoop()
        {
            while (true)
            {

                if (_slideDirection < 0)
                {
                    IncrementIndex();
                    DisplayNext(true);
                }
                else if (_slideDirection > 0)
                {
                    DecrementIndex();
                    DisplayPrevious(true);
                }

                Thread.Sleep(_slideTimer);
            }
        }

        private void SetCursor(Cursor cursor)
        {
            foreach (Control c in Controls)
            {
                if (c != webView21)
                    c.Cursor = cursor;
            }
        }

        private void DisplayNext(bool fromThread = false)
        {
            // Load and display the next image in directory using IsImage helper

            int counter = 0;

            while (!IsValidImage(_files[_currentIndex].FullName))
            {
                IncrementIndex();
                // Loop through all files twice, if no images are found, return
                counter++;
                if (counter >= _files.Count() * 2)
                    return;
            }

            if (_currentIndex < _files.Count())
            {
                LoadImage();
                DisplayImage(fromThread);
            }
        }

        private void DisplayPrevious(bool fromThread = false)
        {
            // Load and display the previous image in directory using IsImage helper

            int counter = 0;

            while (!IsValidImage(_files[_currentIndex].FullName))
            {
                DecrementIndex();
                // Loop through all files twice, if no images are found, return
                counter++;
                if (counter >= _files.Count() * 2)
                    return;
            }

            if (_currentIndex >= 0)
            {
                LoadImage();
                DisplayImage(fromThread);
            }
        }

        private void LoadImage()
        {
            LoadImage(_files[_currentIndex].FullName);
        }

        private void LoadImage(string imagePath)
        {
            HideErrorLabel();
            if (IsValidImage(imagePath))
            {
                var ext = Path.GetExtension(imagePath);
                if (".webp".Equals(ext))
                {
                    LoadWebP(imagePath);
                }
                else if (".webm".Equals(ext))
                {
                    LoadWebM(imagePath);
                }
                else
                {
                    ResetWebView2();

                    try
                    {
                        _currentImage = Image.FromFile(imagePath);
                        DisplayImage(false);
                    }
                    catch (Exception ex)
                    {
                        tlpErrorLabel.Visible = true;
                        lblError.Visible = true;
                        lblError.Text = $"Could not load file '{imagePath}': {ex.Message}";
                    }
                }
            }
            else
            {
                MessageBox.Show($"Unknown image type: '{GetFileType(imagePath)}'");
            }
        }

        private void HideErrorLabel()
        {
            tlpErrorLabel.Visible = false;
            lblError.Visible = false;
            lblError.Text = "";
        }


        private void LoadWebP(string currentImage)
        {
            LoadWebView2(Properties.Resources.webpTemplate, currentImage);
        }

        private void LoadWebM(string currentImage)
        {
            LoadWebView2(Properties.Resources.webmTemplate, currentImage);
        }

        private void LoadWebView2(string htmlTemplate, string currentImage)
        {
            _currentImage = null;
            imageBox1.SetImage(_currentImage);
            imageBox1.Hide();
            webView21.Visible = true;
            var htmlPath = AppDomain.CurrentDomain.BaseDirectory + "currentImage.html";
            var htmlCss = Properties.Resources.style;
            var htmlScript = Properties.Resources.script;
            Debug.WriteLine("current :" + currentImage);
            var htmlContent = string.Format(htmlTemplate, htmlCss, currentImage, htmlScript);
            File.WriteAllText(htmlPath, htmlContent);
            webView21.Source = new Uri(htmlPath);
            webView21.CoreWebView2.Navigate(htmlPath);
            webView21.Enabled = true;
            webView21.Focus();
            //this.Focus();
            UpdateTitle();
        }

        /* Sets current page to blank */
        private void ResetWebView2()
        {
            var htmlPath = AppDomain.CurrentDomain.BaseDirectory + "currentImage.html";
            File.WriteAllText(htmlPath, "<html></html>");
            webView21.Source = new Uri(htmlPath);
            webView21.CoreWebView2.Navigate(htmlPath);
            webView21.Visible = false;
            Focus();
            webView21.Enabled = false;
        }

        private void SetFormText(string text)
        {
            Text = text;
        }

        private void DisplayImage(bool fromThread = false)
        {
            if (_currentImage == null)
                return;
            imageBox1.Show();
            imageBox1.SetImage(_currentImage);
            UpdateTitle(fromThread);

            var currentImage = _files != null ? _files[_currentIndex].FullName : _startFile;
            if (_rotationData.ContainsKey(currentImage))
            {
                imageBox1.ApplyRotation(_rotationData[currentImage]);
            }
        }

        private void UpdateTitle(bool fromThread = false)
        {
            string fileName = GetFileName(_files != null ? _files[_currentIndex].FullName : _startFile);
            bool isWeb = fileName.EndsWith(".webp") || fileName.EndsWith(".webm");
            string scale = isWeb ? "" : $" - { Math.Round(imageBox1.GetCurrentScale() * 100, 0)}%";
            string title = $"{fileName}{scale}";

            if (fromThread)
                Invoke(new FormTextUpdateDelegate(SetFormText), new object[] { title });
            else
                SetFormText(title);
        }

        private void IncrementIndex()
        {
            if (_currentIndex + 1 >= _files.Count())
                _currentIndex = 0;
            else
                _currentIndex++;
        }

        private void DecrementIndex()
        {
            if (_currentIndex - 1 < 0)
                _currentIndex = _files.Count() - 1;
            else
                _currentIndex--;
        }

        public static string GetFileType(string file)
        {
            var parts = file.Split('.');
            return parts.Last();
        }

        private string GetFileType(int index)
        {
            return _files[index].Extension;
        }

        public static bool IsValidImage(string file)
        {
            string type = GetFileType(file);
            return CompExt(type, "gif") || CompExt(type, "png") || CompExt(type, "jpg") || CompExt(type, "jpeg")
                || CompExt(type, "bmp") || CompExt(type, "ico") || CompExt(type, "tif") || CompExt(type, "tiff")
                || CompExt(type, "jfif") || CompExt(type, "webp") || CompExt(type, "webm");
        }

        private static bool CompExt(string input, string check)
        {
            return string.Compare(input, check, true) == 0;
        }


        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            //if (!_ready)
            //    return;

            if ((e.KeyCode == Keys.W || e.KeyCode == Keys.Up) && _modifierKeyPressed)
            {
                StopSlideShow();
                imageBox1.MoveImage(new Point(0, 300));
            }
            else if ((e.KeyCode == Keys.A || e.KeyCode == Keys.Left) && _modifierKeyPressed)
            {
                StopSlideShow();
                imageBox1.MoveImage(new Point(300, 0));
            }
            else if ((e.KeyCode == Keys.S || e.KeyCode == Keys.Down) && _modifierKeyPressed)
            {
                StopSlideShow();
                imageBox1.MoveImage(new Point(0, -300));
            }
            else if ((e.KeyCode == Keys.D || e.KeyCode == Keys.Right) && _modifierKeyPressed)
            {
                StopSlideShow();
                imageBox1.MoveImage(new Point(-300, 0));
            }
            else if (e.KeyCode == Keys.W || e.KeyCode == Keys.Up || e.KeyCode == Keys.Oemplus)
            {
                StopSlideShow();
                imageBox1.Zoom(1);
                UpdateTitle();
            }
            else if (e.KeyCode == Keys.S || e.KeyCode == Keys.Down || e.KeyCode == Keys.OemMinus)
            {
                StopSlideShow();
                imageBox1.Zoom(-1);
                UpdateTitle();
            }
            else if (e.KeyCode == Keys.Space)
            {
                StopSlideShow();
                imageBox1.ZoomToFit();
                UpdateTitle();
            }
            else if (e.KeyCode == Keys.ControlKey)
            {
                _modifierKeyPressed = true;
            }
            else if (e.KeyCode == Keys.Escape)
            {
                StopSlideShow();
            }
            else if (e.KeyCode == Keys.Q)
            {
                StopSlideShow();
                imageBox1.RotateCCW();
                UpdateRotationData();
            }
            else if (e.KeyCode == Keys.E)
            {
                StopSlideShow();
                imageBox1.RotateCW();
                UpdateRotationData();
            }
            // navigation //
            else
                HandleKey(e.KeyCode);
        }

        private void HandleKey(Keys key)
        {
            if (!_ready)
            {
                return;
            }
            else if (key == Keys.Left || key == Keys.A)
            {
                StopSlideShow();
                IncrementIndex();
                DisplayNext();
            }
            else if (key == Keys.Right || key == Keys.D)
            {
                StopSlideShow();
                DecrementIndex();
                DisplayPrevious();
            }
            else if (key == Keys.End)
            {
                StopSlideShow();
                _currentIndex = 0;
                DisplayNext();
            }
            else if (key == Keys.Home)
            {
                StopSlideShow();
                _currentIndex = _files.Count() - 1;
                DisplayPrevious();
            }
            else if (key == Keys.PageDown)
            {
                StopSlideShow();

                if (_currentIndex - 10 >= 0)
                    _currentIndex -= 10;
                else
                    _currentIndex = 0;
                DisplayNext();
            }
            else if (key == Keys.PageUp)
            {
                StopSlideShow();

                if (_currentIndex + 10 < _files.Count())
                    _currentIndex += 10;
                else
                    _currentIndex = _files.Count() - 1;
                DisplayPrevious();
            }
        }

        private void ImageViewerForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ControlKey)
            {
                _modifierKeyPressed = false;
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            _wInfo = new WindowInfo()
            {
                location = this.Location,
                size = this.WindowState == FormWindowState.Normal ? this.Size : _wInfo.size,
            };

            imageBox1.UpdateScale();
            UpdateTitle();
            //DisplayImage();
        }

        private void Form1_MouseWheel(object sender, MouseEventArgs e)
        {
            imageBox1.Zoom(e);
            UpdateTitle();
        }

        private void imageBox1_DoubleClick(object sender, EventArgs e)
        {
            imageBox1.ZoomToFit();
            UpdateTitle();
        }

        private void imageBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _mouseStart = e.Location;
                _mouseHold = true;
            }
            else if (e.Button == MouseButtons.Right)
            {
                UncheckAllItems();

                if (_slideDirection == -1)
                {
                    ((ToolStripMenuItem)contextMenuStrip1.Items[0]).Checked = true;
                    ((ToolStripMenuItem)contextMenuStrip1.Items[2]).Visible = true;

                }
                else if (_slideDirection == 0)
                {
                    ((ToolStripMenuItem)contextMenuStrip1.Items[0]).Enabled = true;
                    ((ToolStripMenuItem)contextMenuStrip1.Items[1]).Enabled = true;
                }
                else if (_slideDirection == 1)
                {
                    ((ToolStripMenuItem)contextMenuStrip1.Items[1]).Checked = true;
                    ((ToolStripMenuItem)contextMenuStrip1.Items[2]).Visible = true;
                }

                contextMenuStrip1.Show(Cursor.Position);
            }

            ResetMouse();
        }

        private void UncheckAllItems()
        {
            for (int i = 0; i < contextMenuStrip1.Items.Count; i++)
            {
                ((ToolStripMenuItem)contextMenuStrip1.Items[i]).Checked = false;
            }

            ((ToolStripMenuItem)contextMenuStrip1.Items[2]).Visible = false;
        }

        private void ResetMouse()
        {
            SetCursor(Cursors.Default);
            _mouseTimer = 0;
        }

        private void imageBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mouseStartHide != Cursor.Position)
            {
                SetCursor(Cursors.Default);
                _mouseTimer = 0;
            }

            if (_mouseHold)
            {
                Point d = new Point(e.Location.X - _mouseStart.X, e.Location.Y - _mouseStart.Y);
                imageBox1.MoveImage(d);
                _mouseStart = e.Location;
            }
        }

        private string GetFileName(string fullPath)
        {
            var fi = new FileInfo(fullPath);
            return fi.Name;
        }

        private string GetDirectory(string fullPath)
        {
            var fi = new FileInfo(fullPath);
            return fi.Directory.FullName;
        }

        private void imageBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _mouseHold = false;
            }
        }

        private struct WindowInfo
        {
            public Point location;
            public Size size;
            public FormWindowState state;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Write window size, state and location to file and use on next startup

            _wInfo = new WindowInfo()
            {
                location = this.Location,
                size = this.WindowState == FormWindowState.Normal ? this.Size : _wInfo.size,
                state = this.WindowState,
            };

            var json = new JavaScriptSerializer().Serialize(_wInfo);
            File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + _settingsFile, json);

            try
            {
                var rotationDataFile = AppDomain.CurrentDomain.BaseDirectory + _rotationDataFile;
                IFormatter formatter = new BinaryFormatter();
                using (Stream stream = new FileStream(rotationDataFile, FileMode.Create, FileAccess.Write))
                {
                    formatter.Serialize(stream, _rotationData);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Could not write rotation data file: " + ex.Message);
            }

        }

        private void UpdateRotationData()
        {
            var currentImage = _files[_currentIndex].FullName;
            RotateFlipType rot = imageBox1.GetRotation();
            if (rot != RotateFlipType.RotateNoneFlipNone)
            {
                if (_rotationData.ContainsKey(currentImage))
                    _rotationData[currentImage] = rot;
                else
                    _rotationData.Add(currentImage, rot);
            }
            else
            {
                if (_rotationData.ContainsKey(currentImage))
                    _rotationData.Remove(currentImage);
            }
        }

        private void stopSlideshowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StopSlideShow();
        }

        private void StopSlideShow()
        {
            _slideDirection = 0;
        }

        private void slowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _slideTimer = 1000;
            _slideDirection = -1;
        }

        private void fastToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _slideTimer = 300;
            _slideDirection = -1;
        }

        private void slowToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            _slideTimer = 1000;
            _slideDirection = 1;
        }

        private void fastToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            _slideTimer = 300;
            _slideDirection = 1;
        }

        private void rotateClockwiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageBox1.RotateCW();
            UpdateRotationData();
            UpdateTitle();
        }

        private void rotateCounterClockwiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageBox1.RotateCCW();
            UpdateRotationData();
            UpdateTitle();
        }

        private void fileInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowProperties.ShowFileProperties(_files[_currentIndex].FullName);
        }

        private void webView21_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void webView21_WebMessageReceived(object sender, CoreWebView2WebMessageReceivedEventArgs e)
        {
            int keyCode = int.Parse(e.WebMessageAsJson);
            HandleKey((Keys)keyCode);
        }
    }
}
