﻿using Microsoft.Web.WebView2.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageLoaderTest
{
    public partial class WebView : Form
    {
        public WebView()
        {
            InitializeComponent();



            //webView21.EnsureCoreWebView2Async();

            //var htmlContent = string.Format(Properties.Resources.webpTemplate, htmlCss, currentImage);
            //File.WriteAllText(htmlPath, htmlContent);

            //webView21.Enabled = true;
        }

        private void webView21_KeyDown(object sender, KeyEventArgs e)
        {
            Debug.WriteLine("webView21_KeyDown " + e.KeyValue);
        }

        private void webView21_CoreWebView2Ready(object sender, EventArgs e)
        {

        }

        private async void WebView_Load(object sender, EventArgs e)
        {
            await webView21.EnsureCoreWebView2Async();
            var htmlPath = AppDomain.CurrentDomain.BaseDirectory + "currentImage.html";
            //var htmlCss = Properties.Resources.style;

            webView21.CoreWebView2.Settings.IsStatusBarEnabled = false;
            webView21.CoreWebView2.Settings.AreDefaultContextMenusEnabled = false;
            webView21.Source = new Uri(htmlPath);
            webView21.CoreWebView2.Navigate(htmlPath);
        }

        private void WebView_KeyUp(object sender, KeyEventArgs e)
        {
            Debug.WriteLine("WebView_KeyUp " + e.KeyValue);
        }

        private void webView21_WebMessageReceived(object sender, CoreWebView2WebMessageReceivedEventArgs e)
        {
            Debug.WriteLine("MyWebView2_WebMessageReceived " + e.WebMessageAsJson);
        }
    }
}
