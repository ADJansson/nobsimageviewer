﻿namespace ImageLoaderTest
{
    partial class ImageViewerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImageViewerForm));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.slideshowLeftToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.slowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.slideshowRightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.slowToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fastToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stopSlideshowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateClockwiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateCounterClockwiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.webView21 = new Microsoft.Web.WebView2.WinForms.WebView2();
            this.imageBox1 = new ImageLoaderTest.ImageBox();
            this.tlpErrorLabel = new System.Windows.Forms.TableLayoutPanel();
            this.lblError = new System.Windows.Forms.Label();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).BeginInit();
            this.tlpErrorLabel.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.slideshowLeftToolStripMenuItem,
            this.slideshowRightToolStripMenuItem,
            this.stopSlideshowToolStripMenuItem,
            this.rotateClockwiseToolStripMenuItem,
            this.rotateCounterClockwiseToolStripMenuItem,
            this.fileInformationToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(211, 136);
            // 
            // slideshowLeftToolStripMenuItem
            // 
            this.slideshowLeftToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.slowToolStripMenuItem,
            this.fastToolStripMenuItem});
            this.slideshowLeftToolStripMenuItem.Name = "slideshowLeftToolStripMenuItem";
            this.slideshowLeftToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.slideshowLeftToolStripMenuItem.Text = "< Slideshow Left";
            // 
            // slowToolStripMenuItem
            // 
            this.slowToolStripMenuItem.Name = "slowToolStripMenuItem";
            this.slowToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.slowToolStripMenuItem.Text = "Slow";
            this.slowToolStripMenuItem.Click += new System.EventHandler(this.slowToolStripMenuItem_Click);
            // 
            // fastToolStripMenuItem
            // 
            this.fastToolStripMenuItem.Name = "fastToolStripMenuItem";
            this.fastToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.fastToolStripMenuItem.Text = "Fast";
            this.fastToolStripMenuItem.Click += new System.EventHandler(this.fastToolStripMenuItem_Click);
            // 
            // slideshowRightToolStripMenuItem
            // 
            this.slideshowRightToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.slowToolStripMenuItem1,
            this.fastToolStripMenuItem1});
            this.slideshowRightToolStripMenuItem.Name = "slideshowRightToolStripMenuItem";
            this.slideshowRightToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.slideshowRightToolStripMenuItem.Text = "Slideshow Right >";
            // 
            // slowToolStripMenuItem1
            // 
            this.slowToolStripMenuItem1.Name = "slowToolStripMenuItem1";
            this.slowToolStripMenuItem1.Size = new System.Drawing.Size(99, 22);
            this.slowToolStripMenuItem1.Text = "Slow";
            this.slowToolStripMenuItem1.Click += new System.EventHandler(this.slowToolStripMenuItem1_Click);
            // 
            // fastToolStripMenuItem1
            // 
            this.fastToolStripMenuItem1.Name = "fastToolStripMenuItem1";
            this.fastToolStripMenuItem1.Size = new System.Drawing.Size(99, 22);
            this.fastToolStripMenuItem1.Text = "Fast";
            this.fastToolStripMenuItem1.Click += new System.EventHandler(this.fastToolStripMenuItem1_Click);
            // 
            // stopSlideshowToolStripMenuItem
            // 
            this.stopSlideshowToolStripMenuItem.Name = "stopSlideshowToolStripMenuItem";
            this.stopSlideshowToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.stopSlideshowToolStripMenuItem.Text = "Stop Slideshow";
            this.stopSlideshowToolStripMenuItem.Click += new System.EventHandler(this.stopSlideshowToolStripMenuItem_Click);
            // 
            // rotateClockwiseToolStripMenuItem
            // 
            this.rotateClockwiseToolStripMenuItem.Name = "rotateClockwiseToolStripMenuItem";
            this.rotateClockwiseToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.rotateClockwiseToolStripMenuItem.Text = "Rotate Clockwise";
            this.rotateClockwiseToolStripMenuItem.Click += new System.EventHandler(this.rotateClockwiseToolStripMenuItem_Click);
            // 
            // rotateCounterClockwiseToolStripMenuItem
            // 
            this.rotateCounterClockwiseToolStripMenuItem.Name = "rotateCounterClockwiseToolStripMenuItem";
            this.rotateCounterClockwiseToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.rotateCounterClockwiseToolStripMenuItem.Text = "Rotate Counter Clockwise";
            this.rotateCounterClockwiseToolStripMenuItem.Click += new System.EventHandler(this.rotateCounterClockwiseToolStripMenuItem_Click);
            // 
            // fileInformationToolStripMenuItem
            // 
            this.fileInformationToolStripMenuItem.Name = "fileInformationToolStripMenuItem";
            this.fileInformationToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.fileInformationToolStripMenuItem.Text = "Properties";
            this.fileInformationToolStripMenuItem.Click += new System.EventHandler(this.fileInformationToolStripMenuItem_Click);
            // 
            // webView21
            // 
            this.webView21.CreationProperties = null;
            this.webView21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webView21.Enabled = false;
            this.webView21.Location = new System.Drawing.Point(0, 0);
            this.webView21.Name = "webView21";
            this.webView21.Size = new System.Drawing.Size(592, 443);
            this.webView21.TabIndex = 1;
            this.webView21.Text = "webView21";
            this.webView21.ZoomFactor = 1D;
            this.webView21.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.webView21.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.webView21_KeyPress);
            this.webView21.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ImageViewerForm_KeyUp);
            this.webView21.WebMessageReceived += new System.EventHandler<Microsoft.Web.WebView2.Core.CoreWebView2WebMessageReceivedEventArgs>(this.webView21_WebMessageReceived);
            this.webView21.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imageBox1_MouseMove);
            // 
            // imageBox1
            // 
            this.imageBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageBox1.Location = new System.Drawing.Point(0, 0);
            this.imageBox1.Name = "imageBox1";
            this.imageBox1.Size = new System.Drawing.Size(592, 443);
            this.imageBox1.TabIndex = 0;
            this.imageBox1.TabStop = false;
            this.imageBox1.DoubleClick += new System.EventHandler(this.imageBox1_DoubleClick);
            this.imageBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imageBox1_MouseDown);
            this.imageBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imageBox1_MouseMove);
            this.imageBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.imageBox1_MouseUp);
            // 
            // tlpErrorLabel
            // 
            this.tlpErrorLabel.BackColor = System.Drawing.Color.Transparent;
            this.tlpErrorLabel.ColumnCount = 3;
            this.tlpErrorLabel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpErrorLabel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpErrorLabel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpErrorLabel.Controls.Add(this.lblError, 1, 1);
            this.tlpErrorLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpErrorLabel.Location = new System.Drawing.Point(0, 0);
            this.tlpErrorLabel.Name = "tlpErrorLabel";
            this.tlpErrorLabel.RowCount = 3;
            this.tlpErrorLabel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpErrorLabel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpErrorLabel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpErrorLabel.Size = new System.Drawing.Size(592, 443);
            this.tlpErrorLabel.TabIndex = 3;
            this.tlpErrorLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imageBox1_MouseMove);
            // 
            // lblError
            // 
            this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblError.AutoSize = true;
            this.lblError.BackColor = System.Drawing.SystemColors.Control;
            this.lblError.Location = new System.Drawing.Point(200, 147);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(191, 147);
            this.lblError.TabIndex = 3;
            this.lblError.Text = "error";
            this.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblError.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imageBox1_MouseMove);
            // 
            // ImageViewerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 443);
            this.Controls.Add(this.tlpErrorLabel);
            this.Controls.Add(this.webView21);
            this.Controls.Add(this.imageBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(190, 160);
            this.Name = "ImageViewerForm";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ImageViewerForm_KeyUp);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).EndInit();
            this.tlpErrorLabel.ResumeLayout(false);
            this.tlpErrorLabel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ImageBox imageBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem slideshowLeftToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem slideshowRightToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopSlideshowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem slowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fastToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem slowToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fastToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rotateClockwiseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateCounterClockwiseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileInformationToolStripMenuItem;
        private Microsoft.Web.WebView2.WinForms.WebView2 webView21;
        private System.Windows.Forms.TableLayoutPanel tlpErrorLabel;
        private System.Windows.Forms.Label lblError;
    }
}

