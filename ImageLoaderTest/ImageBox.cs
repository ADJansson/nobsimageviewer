﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Diagnostics;

namespace ImageLoaderTest
{
    public partial class ImageBox : PictureBox
    {
        private const float _scaleFactor = 1.2f;
        private Matrix _matrix = new Matrix();
        private Matrix _world = new Matrix();
        private bool _zoomedToFit = false;
        private bool _showFullSize = false;
        private Point _currentPos;
        private float _scaleHelper = 1;
        private int _currentRotation = 0;

        public ImageBox()
        {
            InitializeComponent();
        }

        public void SetImage(Image image)
        {
            Image = image;
            _matrix = new Matrix();
            _world = new Matrix();

            _zoomedToFit = false;
            _showFullSize = false;

            UpdateScale();
        }

        //public float GetCurrentScale()
        //{
        //    return _scaleHelper;
        //}

        public void UpdateScale()
        {
            if (Image == null)
                return;

            _matrix = new Matrix();
            _world = new Matrix();

            if (Image.Width > Width || Image.Height > Height)
                _scaleHelper = Math.Min(Width / (float)Image.Width, Height / (float)Image.Height);
            else
            {
                _scaleHelper = 1;
                _showFullSize = true;
            }

            _scaleHelper = _scaleHelper > 1 ? 1 : _scaleHelper;

            //_scaleHelper = 0.9787038f;

            _world.Translate(Width / 2, Height / 2);
            _matrix.Translate(Width / 2, Height / 2);

            _matrix.Scale(_scaleHelper, _scaleHelper);
            _world.Scale(_scaleHelper, _scaleHelper);

            _matrix.Translate(-Width / 2, -Height / 2);
            _world.Translate(-Width / 2, -Height / 2);

            CenterX();
            CenterY();
        }

        public void Zoom(MouseEventArgs e)
        {
            Point p = e.Location;
            int delta = e.Delta;

            Zoom(p, delta);
        }

        public void Zoom(int delta)
        {
            Point p = new Point()
            {
                X = Width / 2,
                Y = Height / 2
            };

            Zoom(p, delta);
        }

        public void Zoom(Point p, int delta)
        {
            // Determine max zoom level
            if (delta > 0)
                if (Math.Pow(GetCurrentScale(), 2) > Math.Pow(_scaleFactor, 30))
                    return;



            if (delta > 0)
            {
                _world.Translate(Width / 2, Height / 2);
                _matrix.Translate(p.X, p.Y);

                _matrix.Scale(_scaleFactor, _scaleFactor);
                _world.Scale(_scaleFactor, _scaleFactor);

                _matrix.Translate(-p.X, -p.Y);
                _world.Translate(-Width / 2, -Height / 2);
            }
            else
            {
                if (GetCurrentScale() > _scaleHelper)
                {
                    _world.Translate(Width / 2, Height / 2);
                    _matrix.Translate(p.X, p.Y);

                    _matrix.Scale(1 / _scaleFactor, 1 / _scaleFactor);
                    _world.Scale(1 / _scaleFactor, 1 / _scaleFactor);

                    _matrix.Translate(-p.X, -p.Y);
                    _world.Translate(-Width / 2, -Height / 2);
                }
                else
                {
                    UpdateScale();
                    return;
                }

            }


            // Move image to center
            //CenterImage();

            if (GetCurrentScale() <= _scaleHelper && !_zoomedToFit)
            {
                CenterImage();
            }

            CenterX();
            CenterY();

            Invalidate();
        }

        public void ZoomToFit()
        {
            if (Image == null)
                return;

            _matrix.Reset();
            _world.Reset();

            if (!_showFullSize)
            {
                _showFullSize = !_showFullSize;
                Invalidate();
                return;
            }
            else if (!_zoomedToFit)
            {
                if (Image.Width < Width || Image.Height < Height)
                {
                    _world.Translate(Width / 2, Height / 2);
                    _matrix.Translate(Width / 2, Height / 2);

                    if (Image.Width < Width)
                    {
                        _scaleHelper = 1 / (Width / (float)Image.Width);
                    }
                    else if (Image.Height < Height)
                    {
                        _scaleHelper = 1 / (Height / (float)Image.Height);
                    }

                    _matrix.Scale(1 / _scaleHelper, 1 / _scaleHelper);
                    _world.Scale(1 / _scaleHelper, 1 / _scaleHelper);

                    _matrix.Translate(-Width / 2, -Height / 2);
                    _world.Translate(-Width / 2, -Height / 2);
                }
                else
                {
                    _showFullSize = !_showFullSize;
                    UpdateScale();
                }
            }
            else
            {
                _showFullSize = !_showFullSize;
                UpdateScale();
            }

            _zoomedToFit = !_zoomedToFit;

            Invalidate();
        }

        public void MoveImage(Point delta)
        {
            _matrix.Translate(delta.X / GetCurrentScale(), delta.Y / GetCurrentScale());

            if (GetCurrentScale() <= _scaleHelper && !_zoomedToFit)
            {
                CenterImage();
            }

            CenterX();
            CenterY();

            Invalidate();
            //WriteMatrix();
        }

        public void CenterImage()
        {
            _matrix.Translate(-_matrix.OffsetX / GetCurrentScale(), -_matrix.OffsetY / GetCurrentScale());
            _matrix.Translate(_world.OffsetX / GetCurrentScale(), _world.OffsetY / GetCurrentScale());

            Invalidate();
        }

        public void CenterX()
        {
            if (Image == null)
                return;

            float iw = Image.Width * GetCurrentScale(); // w(s) = image.W * s * currentScale
            float dw = (Width - iw) / 2f; // legal delta
            float iX = _matrix.OffsetX; // current X pos
            float oX = _world.OffsetX; // "origin" X pos
            float cdw = iX - oX; // current delta
            int cs = cdw < 0 ? 1 : -1;

            //Debug.WriteLine("iw: " + iw + " | dw: " + dw + " | iX: " + iX + " | oX: " + oX + " | cdw: " + cdw);

            if (iw > Width)
            {
                if (Math.Abs(cdw) > Math.Abs(dw))
                {
                    _matrix.Translate(-_matrix.OffsetX / GetCurrentScale(), 0);
                    _matrix.Translate((dw * cs + oX + 1) / GetCurrentScale(), 0);
                    Invalidate();
                }
            }
            else
            {
                _matrix.Translate(-_matrix.OffsetX / GetCurrentScale(), 0);
                _matrix.Translate(_world.OffsetX / GetCurrentScale(), 0);
                Invalidate();

            }
        }

        public void CenterY()
        {
            if (Image == null)
                return;

            float ih = Image.Height * GetCurrentScale(); // w(s) = image.H * s * currentScale
            float dh = (Height - ih) / 2f; // legal delta
            float iY = _matrix.OffsetY; // current Y pos
            float oY = _world.OffsetY - 1.5f * GetCurrentScale(); // "origin" Y pos
            float cdh = iY - oY; // current delta
            int cs = cdh < 0 ? 1 : -1;

            //Debug.WriteLine("iw: " + iw + " | dw: " + dw + " | iX: " + iX + " | oX: " + oX + " | cdw: " + cdw);

            if (ih > Height)
            {
                if (Math.Abs(cdh) > Math.Abs(dh))
                {
                    _matrix.Translate(0, -_matrix.OffsetY / GetCurrentScale());
                    _matrix.Translate(0, (dh * cs + oY + 1) / GetCurrentScale());
                    Invalidate();
                }
            }
            else
            {
                _matrix.Translate(0, -_matrix.OffsetY / GetCurrentScale() - 1);
                _matrix.Translate(0, _world.OffsetY / GetCurrentScale() - 1);
                Invalidate();
            }
        }

        public void ApplyRotation(RotateFlipType rotateFlipType)
        {
            Image.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            Image.RotateFlip(rotateFlipType);
            UpdateScale();
            Invalidate();
            SetInitialRotation(rotateFlipType);
        }

        public void RotateCW()
        {
            Image.RotateFlip(RotateFlipType.Rotate90FlipNone);
            UpdateRotations(90);
            UpdateScale();
            Invalidate();
        }

        public void RotateCCW()
        {
            Image.RotateFlip(RotateFlipType.Rotate270FlipNone);
            UpdateRotations(270);
            UpdateScale();
            Invalidate();
        }

        public void SetInitialRotation(RotateFlipType rotateFlipType)
        {
            if (rotateFlipType == RotateFlipType.RotateNoneFlipNone)
                _currentRotation = 0;
            else if (rotateFlipType == RotateFlipType.Rotate90FlipNone)
                _currentRotation = 90;
            else if (rotateFlipType == RotateFlipType.Rotate180FlipNone)
                _currentRotation = 180;
            else
                _currentRotation = 270;
        }

        private void UpdateRotations(int rotation)
        {
            if (_currentRotation + rotation >= 360)
            {
                _currentRotation = _currentRotation + rotation - 360;
            }
            else
            {
                _currentRotation += rotation;
            }
            Debug.WriteLine("rotation: " + _currentRotation);
        }

        public RotateFlipType GetRotation()
        {
            if (_currentRotation == 0)
                return RotateFlipType.RotateNoneFlipNone;
            else if (_currentRotation == 90)
                return RotateFlipType.Rotate90FlipNone;
            else if (_currentRotation == 180)
                return RotateFlipType.Rotate180FlipNone;
            else
                return RotateFlipType.Rotate270FlipNone;
        }

        public void WriteMatrix()
        {
            Debug.WriteLine("-----------");
            Debug.WriteLine(_matrix.Elements[0] + " " + _matrix.Elements[1] + " " + _matrix.Elements[2]);
            Debug.WriteLine(_matrix.Elements[3] + " " + _matrix.Elements[4] + " " + _matrix.Elements[5]);
            Debug.WriteLine("world");
            Debug.WriteLine(_world.Elements[0] + " " + _world.Elements[1] + " " + _world.Elements[2]);
            Debug.WriteLine(_world.Elements[3] + " " + _world.Elements[4] + " " + _world.Elements[5]);
            Debug.WriteLine("-----------");
        }

        public float GetCurrentScale()
        {
            return _matrix.Elements[0];
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(Brushes.White, 0, 0, Width, Height);
            e.Graphics.Transform = _matrix;
            base.OnPaint(e);
        }
    }
}
