﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace ImageLoaderTest
{
    static class Program
    {

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            bool fromSearch = false;
            string searchName = "";

            if (args != null && args.Length > 0)
            {
                var fileName = args[0];

                if (ImageViewerForm.IsValidImage(fileName))
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new ImageViewerForm(fileName, fromSearch, searchName));
                    //Application.Run(new WebView());
                }
                else
                {
                    MessageBox.Show($"Unknown image type: '{ImageViewerForm.GetFileType(fileName)}'");
                }
            }
        }
    }
}
